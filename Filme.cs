﻿using System;
using System.Collections.Generic;

namespace XDownloader
{
    public class Filme
    {
        public String Nome = "";
        public String NomeOrig = "";
        public String Formato = "";
        public String Genero = "";
        public TimeSpan Tempo;
        public int Ano = 0;
        public String Audio = "";
        public Boolean Legenda = false;
        public Single QuaAudio = 0;
        public Single QuaVideo = 0;
        public int Tamanho = 0;
        public String Capa = "";
        public String Sinopse = "";
        public List<Elenco> Ele = new List<Elenco>();
        public List<Link> Lin = new List<Link>();
    }
}
