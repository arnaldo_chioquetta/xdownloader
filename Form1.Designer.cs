﻿namespace XDownloader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lnkNm1 = new System.Windows.Forms.Label();
            this.lnkNm2 = new System.Windows.Forms.Label();
            this.lnkNm3 = new System.Windows.Forms.Label();
            this.lnkNm4 = new System.Windows.Forms.Label();
            this.txNome = new System.Windows.Forms.TextBox();
            this.txQualidade = new System.Windows.Forms.TextBox();
            this.txLnk1 = new System.Windows.Forms.TextBox();
            this.txLnk2 = new System.Windows.Forms.TextBox();
            this.txLnk3 = new System.Windows.Forms.TextBox();
            this.txLnk4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txGenero = new System.Windows.Forms.TextBox();
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Colar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Location = new System.Drawing.Point(93, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(334, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(433, 9);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Processar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(12, 198);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(88, 55);
            this.webBrowser1.TabIndex = 6;
            this.webBrowser1.Visible = false;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Qualidade:";
            // 
            // lnkNm1
            // 
            this.lnkNm1.Location = new System.Drawing.Point(12, 120);
            this.lnkNm1.Name = "lnkNm1";
            this.lnkNm1.Size = new System.Drawing.Size(111, 16);
            this.lnkNm1.TabIndex = 9;
            this.lnkNm1.Visible = false;
            // 
            // lnkNm2
            // 
            this.lnkNm2.Location = new System.Drawing.Point(12, 146);
            this.lnkNm2.Name = "lnkNm2";
            this.lnkNm2.Size = new System.Drawing.Size(111, 16);
            this.lnkNm2.TabIndex = 10;
            this.lnkNm2.Visible = false;
            // 
            // lnkNm3
            // 
            this.lnkNm3.Location = new System.Drawing.Point(12, 172);
            this.lnkNm3.Name = "lnkNm3";
            this.lnkNm3.Size = new System.Drawing.Size(111, 16);
            this.lnkNm3.TabIndex = 11;
            this.lnkNm3.Visible = false;
            // 
            // lnkNm4
            // 
            this.lnkNm4.Location = new System.Drawing.Point(12, 198);
            this.lnkNm4.Name = "lnkNm4";
            this.lnkNm4.Size = new System.Drawing.Size(111, 16);
            this.lnkNm4.TabIndex = 12;
            this.lnkNm4.Visible = false;
            // 
            // txNome
            // 
            this.txNome.Location = new System.Drawing.Point(127, 52);
            this.txNome.Name = "txNome";
            this.txNome.ReadOnly = true;
            this.txNome.Size = new System.Drawing.Size(381, 20);
            this.txNome.TabIndex = 13;
            this.txNome.Click += new System.EventHandler(this.txNome_Click);
            // 
            // txQualidade
            // 
            this.txQualidade.Location = new System.Drawing.Point(127, 78);
            this.txQualidade.Name = "txQualidade";
            this.txQualidade.ReadOnly = true;
            this.txQualidade.Size = new System.Drawing.Size(46, 20);
            this.txQualidade.TabIndex = 14;
            this.txQualidade.Click += new System.EventHandler(this.txQualidade_Click);
            // 
            // txLnk1
            // 
            this.txLnk1.Location = new System.Drawing.Point(127, 117);
            this.txLnk1.Name = "txLnk1";
            this.txLnk1.ReadOnly = true;
            this.txLnk1.Size = new System.Drawing.Size(381, 20);
            this.txLnk1.TabIndex = 15;
            this.txLnk1.Visible = false;
            this.txLnk1.Click += new System.EventHandler(this.txLnk1_Click);
            // 
            // txLnk2
            // 
            this.txLnk2.Location = new System.Drawing.Point(127, 143);
            this.txLnk2.Name = "txLnk2";
            this.txLnk2.ReadOnly = true;
            this.txLnk2.Size = new System.Drawing.Size(381, 20);
            this.txLnk2.TabIndex = 16;
            this.txLnk2.Visible = false;
            this.txLnk2.Click += new System.EventHandler(this.txLnk2_Click);
            // 
            // txLnk3
            // 
            this.txLnk3.Location = new System.Drawing.Point(127, 169);
            this.txLnk3.Name = "txLnk3";
            this.txLnk3.ReadOnly = true;
            this.txLnk3.Size = new System.Drawing.Size(381, 20);
            this.txLnk3.TabIndex = 17;
            this.txLnk3.Visible = false;
            this.txLnk3.Click += new System.EventHandler(this.txLnk3_Click);
            // 
            // txLnk4
            // 
            this.txLnk4.Location = new System.Drawing.Point(127, 195);
            this.txLnk4.Name = "txLnk4";
            this.txLnk4.ReadOnly = true;
            this.txLnk4.Size = new System.Drawing.Size(381, 20);
            this.txLnk4.TabIndex = 18;
            this.txLnk4.Visible = false;
            this.txLnk4.Click += new System.EventHandler(this.txLnk4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(179, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Gênero:";
            // 
            // txGenero
            // 
            this.txGenero.Location = new System.Drawing.Point(230, 78);
            this.txGenero.Name = "txGenero";
            this.txGenero.ReadOnly = true;
            this.txGenero.Size = new System.Drawing.Size(140, 20);
            this.txGenero.TabIndex = 20;
            this.txGenero.Click += new System.EventHandler(this.txGenero_Click);
            // 
            // webBrowser2
            // 
            this.webBrowser2.Location = new System.Drawing.Point(106, 198);
            this.webBrowser2.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.ScriptErrorsSuppressed = true;
            this.webBrowser2.Size = new System.Drawing.Size(88, 55);
            this.webBrowser2.TabIndex = 21;
            this.webBrowser2.Visible = false;
            this.webBrowser2.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser2_DocumentCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 240);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(520, 22);
            this.statusStrip1.TabIndex = 22;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 262);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.webBrowser2);
            this.Controls.Add(this.txGenero);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txLnk4);
            this.Controls.Add(this.txLnk3);
            this.Controls.Add(this.txLnk2);
            this.Controls.Add(this.txLnk1);
            this.Controls.Add(this.txQualidade);
            this.Controls.Add(this.txNome);
            this.Controls.Add(this.lnkNm4);
            this.Controls.Add(this.lnkNm3);
            this.Controls.Add(this.lnkNm2);
            this.Controls.Add(this.lnkNm1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lnkNm1;
        private System.Windows.Forms.Label lnkNm2;
        private System.Windows.Forms.Label lnkNm3;
        private System.Windows.Forms.Label lnkNm4;
        private System.Windows.Forms.TextBox txNome;
        private System.Windows.Forms.TextBox txQualidade;
        private System.Windows.Forms.TextBox txLnk1;
        private System.Windows.Forms.TextBox txLnk2;
        private System.Windows.Forms.TextBox txLnk3;
        private System.Windows.Forms.TextBox txLnk4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txGenero;
        private System.Windows.Forms.WebBrowser webBrowser2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}

