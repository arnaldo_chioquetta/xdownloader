﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace XDownloader
{
    public static class F
    {
        public static void CarregaFormatos(List<Fmt> TpFormato)
        {
            IEnumerable<XElement> Fmto = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Formatos.xml"))
                                         .Elements("Formatos")
                                         .Elements("tipo");
            foreach (String FF in Fmto)
                TpFormato.Add(new Fmt((String)FF));
        }

        public static void CarregaHospedagens(List<Hosp> Prov)
        {
            IEnumerable<XElement> Hospe = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Hospedagens.xml"))
                                         .Elements("Hospedagens")
                                         .Elements("url");
            foreach (String HH in Hospe)
                Prov.Add(new Hosp((String)HH));
        }

        public static Boolean EncontraLink(String lnk, ref String Tipo, ref String URL, List<Hosp> Prov)
        {
            Boolean Ret = false;
            foreach (Hosp HH in Prov)
            {
                int posN = lnk.IndexOf(HH.URL);
                int posI = lnk.IndexOf(Reverse(HH.URL));
                if ((posN > -1) || (posI > -1))
                {
                    Ret = true;
                    Tipo = HH.URL;
                    if (posN > -1)
                        URL = lnk.Substring(6);
                    else {
                        posN = lnk.IndexOf("?");
                        lnk = lnk.Substring(posN + 1);                        
                        String LinkNormalizado = Reverse(lnk);
                        int PosPontos=0;
                        PosPontos = LinkNormalizado.IndexOf("..");
                        while (PosPontos > 0) {
                            LinkNormalizado = LinkNormalizado.Replace("..", ".");
                            PosPontos = LinkNormalizado.IndexOf("..");
                        }
                        URL = LinkNormalizado.Substring(1);
                    }
                    break;
                }
            }
            return Ret;
        }

        public static String AjustaString(String Texto)
        {
            int pos = 0;

            String Orig = "&amp;";
            String Troca = "&";

            pos = Texto.IndexOf(Orig);
            while (pos > 0)
            {
                Texto = Texto.Replace(Orig, Troca);
                pos = Texto.IndexOf(Orig);
            }
            return Texto;
        }

        public static String Reverse(String s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new String(charArray);
        }

    }
}
