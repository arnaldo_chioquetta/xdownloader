﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace XDownloader
{
    public partial class Form1 : Form
    {

        enum TStatus
        {
            SemAtividade,
            PaginaOrigem,
            PaginaLink,
            PaginaDownload
        };

        private TStatus lcStatus = TStatus.SemAtividade;
        private String UltUrl = "";
        //private System.Drawing.Color CorNormal = null;

        private TStatus getStatus()
        {
            return lcStatus;
        }

        private void setStatus(TStatus ts)
        {
            lcStatus = ts;
            String Aux="";
            switch (lcStatus)
            {
                case TStatus.PaginaOrigem:
                    {
                        Aux = "Carregando página da Telona.org";
                    }
                    break;
                    /* case TStatus.PaginaLink:
                    {
                        Aux = "Carregando página do Link";
                    }
                    break;
                case TStatus.PaginaDownload:
                    {
                        Aux = "Carregando página do Download";
                    }
                    break; */
            }
            toolStripStatusLabel1.Text = Aux;
        }

        public Form1()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            IDataObject iData = Clipboard.GetDataObject();
            if (iData.GetDataPresent(DataFormats.Text))
            {
                String str = (String)iData.GetData(DataFormats.Text);
                textBox1.Text = str;
            }
            lnkNm1.Visible = false;
            txLnk1.Visible = false;
            lnkNm2.Visible = false;
            txLnk2.Visible = false;
            lnkNm3.Visible = false;
            txLnk3.Visible = false;
            lnkNm4.Visible = false;
            txLnk4.Visible = false;
            txNome.Text = "";
            txQualidade.Text = "";
            txGenero.Text = "";
            this.Text = "XDownloader";            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            String Endereco = textBox1.Text;
            System.Uri Ender = new System.Uri(Endereco);
            webBrowser1.Url = Ender;
            setStatus(TStatus.PaginaOrigem);
            webBrowser1.Navigate(Endereco);
            textBox1.Text = "";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            button2.Enabled = (textBox1.Text.IndexOf("http://")==0);
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (UltUrl != webBrowser1.Url.ToString())
            {
                UltUrl = webBrowser1.Url.ToString();
                String TextoPagina = webBrowser1.Document.Body.OuterHtml;
                Telona Pag = new Telona();
                Pag.Processa(TextoPagina);
                toolStripStatusLabel1.Text = "";
                ColocaDadosNaTela(Pag);

                /*Filme F = CopiaClasse(Pag);
                SerializeToXML(F);
                String URL = EscolherLnk(Pag);

                System.Uri Ender = new System.Uri(URL);
                webBrowser2.Url = Ender;
                setStatus(TStatus.PaginaLink);
                webBrowser2.Navigate(URL);
                 */

            }

        }

        private void ColocaDadosNaTela(Filme F)
        {
            txNome.Text = F.Nome;
            txQualidade.Text = F.QuaAudio.ToString();
            txGenero.Text = F.Genero;
            if (F.Formato == "Formato Desconhecido!")
                this.Text = "Formato Desconhecido!";
            else
                this.Text = "XDownloader";
            StringBuilder Aspa = new StringBuilder("");
            Aspa.Append((char)34);
            for (int a = 0; a < F.Lin.Count; a++)
            {
                String EsseLink = F.Lin[a].URL.ToString().Replace(Aspa.ToString(), "");
                String Hosp = F.Lin[a].Tipo.ToString();

                Label L=null;
                TextBox T=null;
                switch (a)
                {
                    case 0:
                        {
                            L = lnkNm1;
                            T = txLnk1;
                        }
                        break;
                    case 1:
                        {
                            L = lnkNm2;
                            T = txLnk2;
                        }
                        break;
                    case 2:
                        {
                            L = lnkNm3;
                            T = txLnk3;
                        }
                        break;
                    case 3:
                        {
                            L = lnkNm4;
                            T = txLnk4;
                        }
                        break;
                }
                //L.BackColor = CorNormal;
                //T.BackColor = CorNormal;
                L.Visible = true;
                T.Visible = true;
                L.Text = Hosp;
                T.Text = EsseLink;
            }
        }

        private void SerializeToXML(Filme F)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Filme));
            TextWriter textWriter = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Filmes.xml"),true);
            serializer.Serialize(textWriter, F);
            textWriter.Close();
        }

        private String EscolherLnk(Filme F)
        {
            // Este método deve escolher qual é o link ideal para download
            // Primeiro critério deve ser selecionar os link's que podem ser baixados agora
            // considerando que alguns provedores tem uma limitação de tempo para um segundo download
            // Dentre os possívels deve retornar oq que baixa mais rápido

            // Indicar o link que foi utilizado para download
            int Escolha = 0;

            Label L = null;
            TextBox T = null;
            switch (Escolha)
            {
                case 0:
                    {
                        L = lnkNm1;
                        T = txLnk1;
                    }
                    break;
                case 1:
                    {
                        L = lnkNm2;
                        T = txLnk2;
                    }
                    break;
                case 2:
                    {
                        L = lnkNm3;
                        T = txLnk3;
                    }
                    break;
                case 3:
                    {
                        L = lnkNm4;
                        T = txLnk4;
                    }
                    break;
            }
            //L.BackColor = System.Drawing.Color.Yellow;
            //T.BackColor = System.Drawing.Color.Yellow;
            return F.Lin[Escolha].URL.ToString();
        }

        private Filme CopiaClasse(Telona P)
        {
            Filme F = new Filme();
            F.Nome = P.Nome;
            F.NomeOrig = P.NomeOrig;
            F.Formato = P.Formato;
            F.Genero = P.Genero;
            F.Tempo=P.Tempo;
            F.Ano = P.Ano;
            F.Audio = P.Audio;
            F.Legenda = P.Legenda;
            F.QuaAudio = P.QuaAudio;
            F.QuaVideo = P.QuaVideo;
            F.Tamanho = P.Tamanho;
            F.Capa = P.Capa;
            F.Sinopse = P.Sinopse;
            F.Ele = P.Ele;
            F.Lin = P.Lin;
            return F;
        }

        private void txLnk1_Click(object sender, EventArgs e)
        {
            Clicado(txLnk1);
        }

        private void txLnk2_Click(object sender, EventArgs e)
        {
            Clicado(txLnk2);
        }

        private void txLnk3_Click(object sender, EventArgs e)
        {
            Clicado(txLnk3);
        }

        private void txLnk4_Click(object sender, EventArgs e)
        {
            Clicado(txLnk4);
        }

        private void txQualidade_Click(object sender, EventArgs e)
        {
            Clicado(txQualidade);
        }

        private void txNome_Click(object sender, EventArgs e)
        {
            Clicado(txNome);
        }

        private void Clicado(TextBox Objeto)
        {
            Objeto.SelectAll();
            Clipboard.SetDataObject(Objeto.SelectedText);            
            System.Windows.Forms.ToolTip ToolTip1 = new System.Windows.Forms.ToolTip();
            ToolTip1.SetToolTip(Objeto, "Copiado para memória");
        }

        private void txGenero_Click(object sender, EventArgs e)
        {
            Clicado(txGenero);
        }

        private void webBrowser2_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (UltUrl != webBrowser2.Url.ToString())
            {
                UltUrl = webBrowser2.Url.ToString();
                String TextoPagina = webBrowser2.Document.Body.OuterHtml;
                int x = 0;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //CorNormal = lnkNm1.BackColor;
            toolStripStatusLabel1.Text = "";
        }

    }
}
