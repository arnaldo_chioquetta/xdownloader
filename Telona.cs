﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace XDownloader
{
    public class Telona : Filme
    {
        private String TextoPagina;
        private List<Fmt> TpFormato = new List<Fmt>();
        private List<Hosp> Prov = new List<Hosp>();

        public Telona()
        {
            F.CarregaFormatos(TpFormato);
            F.CarregaHospedagens(Prov);            
        }

        public void Processa(String TextoPagina)
        {
            this.TextoPagina = TextoPagina; 
            
            int ini = TextoPagina.IndexOf("Permanent Link to", 0);
            String Texto = TextoPagina.Substring(ini + 18, 150);
            int fim = Texto.IndexOf("http://");
            Texto = Texto.Substring(0, fim);

            foreach (Fmt Forma in TpFormato) 
            {
                String Tp = Forma.Tipo;
                ini = Texto.IndexOf(Forma.Tipo);
                if (ini > 0)
                {
                    Formato = Forma.Tipo;
                    Nome = F.AjustaString(Texto.Substring(0, ini-1));
                    break;
                }
            }
            if (Formato == "")
                Formato="Formato Desconhecido!";

            NomeOrig = Coleta("Título Original",Tam:70);
            Genero = Coleta("Gênero");
            Tempo = ConvMimTempo(Coleta("Tempo de Duração"));
            String sAno = Coleta("Ano de Lançamento");
            int PosEspaco=sAno.IndexOf(" ");
            if (PosEspaco > 0)
                sAno = sAno.Substring(0, PosEspaco);                    
            Ano = int.Parse(sAno);
            QuaAudio = Single.Parse(Coleta("Qualidade de Audio",Tam:150));
            QuaVideo = Single.Parse(Coleta("Qualidade de Vídeo"));
            Sinopse = Coleta("Sinopse","</P>",1500);

            // Audio & Legenda
            Audio = Coleta("Áudio");
            if (Audio.IndexOf("Português") > -1)
            {
                Audio = "Português";
                Legenda = false;
            }
            else
                Legenda = true;

            // Tamanho
            String sTamanho = Coleta("Tamanho", " Mb");
            ini = sTamanho.IndexOf("|");            

            String StrNum = sTamanho.Substring(ini + 2);
            int iFim = StrNum.IndexOf("–");
            if (iFim > 0) {
                StrNum = StrNum.Substring(0,iFim); }
            
            Tamanho = int.Parse(StrNum);

            // Poster
            ini=TextoPagina.IndexOf("Filme Poster");
            Capa = TextoPagina.Substring(ini, 300);
            ini=Capa.IndexOf("src=");
            Capa = Capa.Substring(ini + 5);
            fim = Capa.IndexOf("</DIV>");
            if (fim == -1)
                fim = Capa.IndexOf("&amp;container=");
            else
                fim = fim - 2;
            Capa = Capa.Substring(0, fim);

            // Elenco
            String sElenco = Coleta("Elenco", "</P>", 2000).Substring(3);
            String[] aElenco = sElenco.Split(new char[] { '<' }, StringSplitOptions.None);
            for (int a = 0; a < aElenco.Count(); a++) {
                sElenco = aElenco[a].Replace("BR>", "");
                ini = sElenco.IndexOf(" … ");
                String Ator = "";
                String Perso = "";
                if (ini > -1) {
                    Ator = sElenco.Substring(0, ini);
                    Perso = sElenco.Substring(ini + 3);
                }
                else {
                    Ator = Perso = sElenco;
                }

                Elenco Elen = new Elenco();
                Elen.Ator = Ator;
                Elen.Perso = Perso;
                Ele.Add(Elen);
            }
            ini = TextoPagina.IndexOf("<STRONG>RMVB");
            String sLinks = TextoPagina.Substring(ini, 1200);
            ini = sLinks.IndexOf("href");
            sLinks = sLinks.Substring(ini);
            fim = sLinks.IndexOf("</P>");
            sLinks = sLinks.Substring(0, fim);
            int cont = 1;
            Boolean Sair = false;
            while ((ini > 0) || (Sair=false)) {
                fim = sLinks.IndexOf("target");                    
                String lnk = sLinks.Substring(0, fim - 1);
                if (lnk.IndexOf("http") > 0) {
                    String Tipo = "";
                    String URL = "";
                    if (F.EncontraLink(lnk, ref Tipo, ref URL, Prov)) {
                        Link LNK = new Link();
                        LNK.Tipo = Tipo;
                        LNK.URL = URL;
                        Lin.Add(LNK);
                    }
                    else {
                        MessageBox.Show("Hospedagem Desconhecida! : " + lnk + " ");
                        Sair = true;
                        break;
                    }
                }
                if (Sair == false) { 
                    sLinks = sLinks.Substring(lnk.Length + 1);
                    ini = sLinks.IndexOf("href");
                    if (ini > 0) {
                        sLinks = sLinks.Substring(ini);
                        cont++;
                    }
                }
            }


        }

        private String Coleta(String sINI, String sFim="", int Tam=50)
        {
            if (sFim == "")
                sFim = "<BR>";
            sINI += ":</STRONG>";
            int ini = TextoPagina.IndexOf(sINI);
            String Str = TextoPagina.Substring(ini + sINI.Length+1, Tam);
            int fim = Str.IndexOf(sFim);
            Str = Str.Substring(0, fim);
            fim = Str.IndexOf("<SPAN");
            if (fim > 0) {
                Str = Str.Substring(0, fim);
                return Str;
            }
            else {
                return F.AjustaString(Str);
            }
            
        }

        private TimeSpan ConvMimTempo(String Texto)
        {
            int Pos = Texto.IndexOf(" min");
            String T = Texto.Substring(0, Pos);
            int M = int.Parse(T);
            int H=M/60;
            M=M % 60;
            TimeSpan Tempo = new TimeSpan(H, M, 0);
            return Tempo;
        }
    }

}
